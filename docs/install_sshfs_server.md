# Setting Up the SSHFS Server

This guide will walk you through the process of setting up an SSHFS server,
which is used for storing and backing up
data. This server requires two partitions: one for storage and one for backups.
Additionally, BorgBackup is used for
data backup.

## Prerequisites

- A server running Debian.
- Two partitions available for storage and backup.
- Public SSH keys for the staging and live servers.
- MySQL server credentials (IP, username, password, port).

## Steps to Set Up the SSHFS Server

### Step 1: Generate SSH Keys

Refer to
the [Generating and managing SSH Keys for DDEV Deployment](ssh-key-setup.md)
guide to generate the necessary
SSH keys.

### Step 2: Download and Execute the Installation Script

Use `curl` or `wget` to download the installation script to your server, then
execute it. Below is an example of how to
do this for the SSHFS server.

#### Using `curl`

```bash
curl -O https://gitlab.com/mdys/ddev-deployment-automation/-/raw/main/scripts/install/install.server.storage.sh
bash install.server.storage.sh
```

#### Using `wget`

```bash
wget https://gitlab.com/mdys/ddev-deployment-automation/-/raw/main/scripts/install/install.server.storage.sh
bash install.server.storage.sh
```

### Step 3: Follow the Prompts

The script will prompt you for the following information:

- Enter the public SSH key for the staging server.
- Enter the public SSH key for the live server.
- Enter the MySQL server IP.
- Enter the MySQL server user.
- Enter the MySQL server password.
- Enter the MySQL server port (default is 3306).
- Enter the partition for storage (/dev/sda or similar).
- Enter the partition for backups (/dev/sdb or similar).

#### Example Input

```bash
Enter the public SSH key for the staging server: <your_ssh_public_key_staging>
Enter the public SSH key for the live server: <your_ssh_public_key_live>
Enter the MySQL server IP: <your_mysql_server_ip>
Enter the MySQL server user: <your_mysql_user>
Enter the MySQL server password: <your_mysql_password>
Enter the MySQL server port: 3306
Enter the partition for storage: /dev/sda1
Enter the partition for backups: /dev/sdb1
```

### Step 4: Verify and Confirm

The script will output the entered values and ask for confirmation. Verify the
values and confirm to proceed.

### Step 5: Script Execution

The script will then:

- Install necessary packages.
- Format the specified partitions.
- Mount the partitions.
- Initialize BorgBackup repository.
- Set up SSH keys for the storage user.
- Configure MySQL client access.
- Set up cron jobs for BorgBackup.

### Important Aspects

#### Partition Setup

The script formats and mounts the specified partitions for storage and backup.
The partitions are added to `/etc/fstab`
to ensure they are mounted on boot.

#### BorgBackup Configuration

BorgBackup is used to create and prune backups. The script initializes a
BorgBackup repository and sets up cron jobs to
automate the backup process.

#### MySQL Configuration

The MySQL credentials are configured in `/home/storage/.my.cnf` for easy access
by the storage user.

#### SSH Key Setup

The public SSH keys for the staging and live servers are added
to `/home/storage/.ssh/authorized_keys` to allow
connections from these servers.

### Understanding SSH Keys and GitLab Runner Usage

- **SSH_KEY_PRIVATE_STAGING**: Stored in GitLab CI/CD variables, used by the
  GitLab Runner to authenticate and connect
  to the stage server.
- **SSH_KEY_PUBLIC_STAGING**: Added to the stage server to allow connections
  from the GitLab Runner.
- **Public Key Generated on the Server**: This key, generated during the
  installation process, is used to connect to the
  SSHFS server.

These keys ensure secure communication between your GitLab CI/CD pipeline and
the servers. After setting up the server,
the public key generated on the server will be used to grant access to the SSHFS
server, and the private key stored in
GitLab will be used by the pipeline for SSH authentication.

### Example Configuration for GitLab CI/CD

In your GitLab CI/CD settings, ensure the necessary variables are defined as
mentioned earlier. This will automate the
deployment process across your environments.

### Conclusion

By following these steps, you can set up and utilize DDEV deployment automation
scripts on your SSHFS server, ensuring
efficient and reliable storage and backup of your data. The provided
installation script simplifies the setup process,
and the environment variables defined in GitLab CI/CD settings ensure
consistency across your projects.
