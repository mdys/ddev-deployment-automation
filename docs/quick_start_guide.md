# Quick Start Guide

This guide provides a step-by-step overview of setting up and using the DDEV
Deployment Automation project.

## Prerequisites

1. **GitLab Account**: Ensure you have a GitLab account.
2. **GitLab Group**: Create a group in GitLab where your projects will be
   managed.
3. **Infrastructure**: Purchase three servers that are accessible over the
   internet. These will be used for the live
   environment, staging environment, and SSHFS server.

## Steps

### 1. Define Variables in GitLab Group

1. Go to your GitLab group settings.
2. Navigate to CI/CD > Variables.
3. Define the necessary variables for your environment. These variables will be
   used by the deployment scripts.

### 2. Purchase Servers

1. **Live Server**: This server will host your production environment.
2. **Staging Server**: This server will host your staging environment.
3. **SSHFS Server**: This optional server will manage SSHFS mounts for
   separating content from the source code.

### 3. Install Server Dependencies

1. Use the installation scripts from
   the `ddev-deployment-automation/scripts/install` directory.
2. Run the appropriate script on each server:

   - `install.server.ddev.live.sh` for the live server.
   - `install.server.ddev.stage.sh` for the staging server.
   - `install.server.storage.sh` for the SSHFS server.

3. Follow the prompts provided by the installation scripts. They will guide you
   through the setup process. Note that the
   scripts require Debian.

### 4. Create a GitLab Project

1. Create a new project in GitLab within your group.
2. Assign the project to the group where the variables have been defined.

### 5. Initialize DDEV Locally

1. Clone your GitLab project repository to your local machine.
2. Run `ddev config` to set up your DDEV environment.
3. Ensure that DDEV is running correctly by using `ddev start`.
4. Configure your `.ddev/project.yaml` file with the necessary settings for your
   environment.

### 6. Set Up GitLab CI/CD

1. Create a `.gitlab-ci.yml` file in your project repository.
2. Include the DDEV deployment automation
   configuration:`https://gitlab.com/mdys/staging-test-project`
3. Commit and push the changes to your repository.

### 7. Create a Merge Request

1. Make changes to your codebase and push them to a new branch.
2. Create a Merge Request for your changes.
3. The Merge Request will trigger the creation of a unique staging environment
   with its own domain.
4. The environment will be accessible directly from the Merge Request in GitLab.

### 8. Merge to Main

1. Once the Merge Request is reviewed and approved, merge it into the `main`
   branch.
2. Merging into `main` will trigger the deployment of the live environment.
3. The live environment will be built using the primary domain specified in your
   DDEV configuration.

### 9. Managing Environments

1. Multiple domains and subdomains can be used for your environments.
2. GitLab will create environments for each Merge Request, making them easily
   accessible from the Merge Request
   interface.
3. Environments can be managed directly from the GitLab CI/CD settings.

By following these steps, you can effectively set up and utilize the DDEV
Deployment Automation project, ensuring smooth
and efficient deployments across your development, staging, and production
environments.
