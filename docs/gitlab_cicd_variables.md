# GitLab CI/CD Variables

In order to automate the deployment process using DDEV Deployment Automation,
you need to set up specific environment
variables in your GitLab group settings. These variables ensure that all
projects within the group can utilize the same
configuration without requiring individual project-specific settings.

## Setting Up Variables in GitLab

1. Navigate to your GitLab group.
2. Go to **Settings** > **CI/CD** > **Variables**.
3. Add the following variables:

## List of Variables

### DDEV_LIVE_SERVER

- **Description**: The hostname or IP address of the live server.
- **Usage**: Used to SSH into the live server for deployment.

### DDEV_STAGE_SERVER

- **Description**: The hostname or IP address of the staging server.
- **Usage**: Used to SSH into the staging server for deployment.

### DDEV_STAGE_USER

- **Description**: The username for accessing the staging server.
- **Usage**: Used for SSH authentication on the staging server.

### GITLAB_ACCESS_TOKEN

- **Description**: The access token for GitLab API.
- **Usage**: Used for accessing secure files and cloning repositories.

### MYSQL_SERVER_IP

- **Description**: The IP address of the MySQL server.
- **Usage**: Used for database connections in the deployment scripts.

### MYSQL_SERVER_PASSWORD

- **Description**: The password for the MySQL server user.
- **Usage**: Used for authenticating to the MySQL server.

### MYSQL_SERVER_PORT

- **Description**: The port number for the MySQL server.
- **Usage**: Used for connecting to the MySQL server.

### MYSQL_SERVER_USER

- **Description**: The username for the MySQL server.
- **Usage**: Used for database connections in the deployment scripts.

### SSH_KEY_PRIVATE_STAGING

- **Description**: The private SSH key for accessing the staging server.
- **Usage**: Used for SSH authentication on the staging server.

### SSH_KEY_PUBLIC_STAGING

- **Description**: The public SSH key for accessing the staging server.
- **Usage**: Used for SSH authentication on the staging server.

### STAGING_DOMAIN

- **Description**: The domain name for the staging environment.
- **Usage**: Used to construct the URL for the staging environment.

### STAGING_STORAGE_IP

- **Description**: The IP address of the staging storage server.
- **Usage**: Used for SSHFS mounts and file storage operations.

### STAGING_STORAGE_USER

- **Description**: The username for accessing the staging storage server.
- **Usage**: Used for SSH authentication on the staging storage server.

## How These Variables Are Used

### General Usage

- These variables are used across various scripts to manage the deployment
  process.
- They ensure that the necessary credentials and configurations are available
  without hardcoding sensitive information
  into the scripts.

### SSH Authentication

- `DDEV_STAGE_USER`, `SSH_KEY_PRIVATE_STAGING`, and `SSH_KEY_PUBLIC_STAGING` are
  used to establish SSH connections to the staging server.
- Similarly, `DDEV_LIVE_SERVER` and related variables are used for the live
  server.

### Database Configuration

- `MYSQL_SERVER_IP`, `MYSQL_SERVER_PORT`, `MYSQL_SERVER_USER`,
  and `MYSQL_SERVER_PASSWORD` are used to connect to the
  MySQL database for operations such as backups and data migrations.

### GitLab API Access

- `GITLAB_ACCESS_TOKEN` is used to interact with the GitLab API, allowing the
  scripts to clone repositories, access
  secure files, and perform other GitLab-related operations.

### Domain and Storage Management

- `STAGING_DOMAIN` is used to construct URLs for the staging environment.
- `STAGING_STORAGE_IP` and `STAGING_STORAGE_USER` are used to manage SSHFS
  mounts for storage.

By setting up these variables in your GitLab group, you ensure a consistent and
secure configuration that can be utilized by all projects within the group,
streamlining the deployment process
and reducing the need for repetitive setup.
