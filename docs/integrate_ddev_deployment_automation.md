# Integrating DDEV Deployment Automation into Your GitLab Project

To integrate the DDEV Deployment Automation scripts into your own GitLab
project, follow the steps outlined below. This guide assumes your project is
within a GitLab group where the necessary CI/CD variables are already defined.

## Prerequisites

1. **GitLab Project**: Ensure you have a GitLab project within a group that has
   the required CI/CD variables set up.
2. **DDEV Configuration**: Your project should be configured to use DDEV
   locally.

## Step-by-Step Integration

### Step 1: Define Variables

At the beginning of your .gitlab-ci.yml file, you can define the variable
DDEV_SCRIPT_VERSION. This variable will be used to check out the Git repository
of this project. You can use tags or branches. If the value does not exist or
the name is incorrect, main will be used as a fallback.

```yaml
variables:
  DDEV_SCRIPT_VERSION: "v1.1.1" # (This is a tag. You can also use branches)
```

### Step 2: Include the DDEV Deployment Automation Configuration

Next, include the DDEV Deployment Automation configuration file. This file
contains the predefined CI/CD jobs that handle the deployment process.

```yaml
include:
  - remote: 'https://gitlab.com/mdys/ddev-deployment-automation/-/raw/main/ddev.staging.yaml'
```

### Step 3: Define Stages

Define the stages for your CI/CD pipeline. Typically, you will need at least
the `build` and `deploy` stages.

```yaml
stages:
  - build
  - deploy
```

### Step 4: Extend the Predefined Jobs

Extend the predefined jobs provided by the DDEV Deployment Automation
configuration to fit your project needs.

#### Deploy to Staging Environment

This job handles the deployment to the staging environment. It is triggered on
merge requests.

```yaml
staging:deploy:review:
  stage: deploy
  extends: .ddev_deploy_review
```

#### Deploy to Live Environment

This job handles the deployment to the live environment. It is triggered when
changes are merged into the `main` branch.

```yaml
staging:deploy:live:
  stage: deploy
  extends: .ddev_deploy_live
```

### Example `.gitlab-ci.yml` File

Below is a complete example of what your `.gitlab-ci.yml` file might look like
after integrating the DDEV Deployment Automation scripts:

```yaml
variables:
  DDEV_SCRIPT_VERSION: "v1.1.1" # (This is a tag. You can also use branches)

include:
  - remote: 'https://gitlab.com/mdys/ddev-deployment-automation/-/raw/main/ddev.staging.yaml'

stages:
  - build
  - deploy

staging:deploy:review:
  stage: deploy
  extends: .ddev_deploy_review

staging:deploy:live:
  stage: deploy
  extends: .ddev_deploy_live
```

## Additional Notes

- **CI/CD Variables**: Ensure all necessary CI/CD variables are set up in your
  GitLab group settings. These variables include server details, SSH keys,
  database credentials, etc. Refer to
  the [GitLab CI/CD Variables](docs/gitlab_cicd_variables.md) documentation for
  a comprehensive list of required variables.
- **DDEV Configuration**: Your project should already be configured to use DDEV
  locally. Ensure that your `.ddev/config.yaml` file is properly set up.
- **Environment Management**: The predefined jobs will handle creating,
  deploying, and removing environments as needed. Review
  the [Project Overview](docs/project_overview.md)
  and [Quick Start Guide](docs/quick_start_guide.md) for more details on how
  environments are managed.

By following these steps, you can seamlessly integrate the DDEV Deployment
Automation scripts into your GitLab project, ensuring efficient and reliable
deployments across your staging and live environments.
