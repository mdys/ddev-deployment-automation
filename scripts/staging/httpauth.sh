#!/bin/bash

echo "Running scripts/staging/httpauth.sh"

# Check necessary variables
check_variable "$PROJECT_CONFIG_FILE" "PROJECT_CONFIG_FILE"
check_variable "$PROJECT_FOLDER" "PROJECT_FOLDER"

if [ -f "$PROJECT_CONFIG_FILE" ]; then
  # Get values from .ddev/project.yaml
  HTTP_AUTH_STAGING=$( shyaml -q get-value http-auth-staging < "$PROJECT_CONFIG_FILE" )

  if [ "$HTTP_AUTH_STAGING" == "true" ]; then
    HTTP_AUTH_STAGING_USERNAME=$( shyaml -q get-value http-auth-staging-username < "$PROJECT_CONFIG_FILE" )
    HTTP_AUTH_STAGING_PASSWORD=$( shyaml -q get-value http-auth-staging-password < "$PROJECT_CONFIG_FILE" )

    htpasswd -b -c "$PROJECT_FOLDER/.htpasswd" "$HTTP_AUTH_STAGING_USERNAME" "$HTTP_AUTH_STAGING_PASSWORD" || error_exit "Failed to create htpasswd"

# Construct the .htaccess path correctly
  HTACCESS_PATH="$PROJECT_FOLDER"
  if [ -n "$DDEV_DOCROOT" ]; then  # Check if DDEV_DOCROOT is not empty
    HTACCESS_PATH+="/$DDEV_DOCROOT"
  fi
  HTACCESS_PATH+="/.htaccess"

  cat <<EOT >> "$HTACCESS_PATH"
Authtype Basic
AuthName "Auth"
AuthUserFile /var/www/html/.htpasswd
Require valid-user
EOT

    # Deal with tests even when http auth is activated. @todo: move to typo3.sh
    if [ "$DDEV_TYPE" == "typo3" ]; then
      sed -i "s/https:\/\//https:\/\/$HTTP_AUTH_STAGING_USERNAME:$HTTP_AUTH_STAGING_PASSWORD@/g" "$PROJECT_FOLDER/codecept.conf.js" || error_exit "Failed to update codecept.conf.js"
    fi
  fi
fi
