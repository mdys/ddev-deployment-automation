#!/bin/bash

echo "Running scripts/staging/ddevcreate.sh"

# Check necessary variables
check_variable "$PROJECT_FOLDER" "PROJECT_FOLDER"
check_variable "$PROJECT_SLUG" "PROJECT_SLUG"
check_variable "$ALL_DDEV_DOMAINS" "ALL_DDEV_DOMAINS"
check_variable "$DEPLOY_TYPE" "DEPLOY_TYPE"

cd /home/ddev || error_exit "Failed to change directory to /home/ddev"
# Make sure that ddev global settings are correct
ddev config global --router-bind-all-interfaces --use-letsencrypt --letsencrypt-email=der.raecher@gmail.com --router nginx-proxy || error_exit "Failed to configure ddev global settings"
cd "$PROJECT_FOLDER" || error_exit "Failed to change directory to $PROJECT_FOLDER"
echo "Change ddev config"
ddev config --project-name "$PROJECT_SLUG" || error_exit "Failed to configure ddev project name"
ddev config --additional-fqdns "$ALL_DDEV_DOMAINS" || error_exit "Failed to configure additional fqdns"
# Omit ddev db container which is not needed when deploying to live
if [ "$DEPLOY_TYPE" == "live" ]; then
  # No database and phpmyadmin needed in live environment
  ddev config --omit-containers db,ddev-ssh-agent || error_exit "Failed to configure omitted containers for live environment"
else
  if [ -n "$NO_DATABASE" ]; then
    echo "Omitting database container because no-database inside project.yaml is set to true."
    ddev config --omit-containers db,ddev-ssh-agent || error_exit "Failed to configure omitted containers for review environment"
  else
    ddev config --omit-containers ddev-ssh-agent || error_exit "Failed to configure omitted containers for review environment"
  fi
fi
