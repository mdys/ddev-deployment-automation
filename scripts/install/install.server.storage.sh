#!/usr/bin/env bash

# Prompt for necessary information
echo "Enter the public SSH key for the staging server:"
read -r SSH_KEY_PUBLIC_STAGING

echo "Enter the public SSH key for the live server:"
read -r SSH_KEY_PUBLIC_LIVE

echo "Enter the MySQL server IP:"
read -r MYSQL_SERVER_IP

echo "Enter the MySQL server user:"
read -r MYSQL_SERVER_USER

echo "Enter the MySQL server password:"
read -r MYSQL_SERVER_PASSWORD

echo "Enter the MySQL server port:"
read -r MYSQL_SERVER_PORT

echo "Enter the partition for storage (/dev/sda or similar):"
read -r PARTITION_STORAGE

echo "Enter the partition for backups (/dev/sdb or similar):"
read -r PARTITION_BACKUP

# Install necessary packages
apt-get update
apt-get install -y rsync default-mysql-client borgbackup htop

# Format and mount partitions
mkfs.ext4 "$PARTITION_STORAGE"
mkdir -p /mnt/storage
mkfs.ext4 "$PARTITION_BACKUP"
mkdir -p /var/borg
echo "$PARTITION_STORAGE /mnt/storage ext4 defaults 0 0" >> /etc/fstab
echo "$PARTITION_BACKUP /var/borg ext4 defaults 0 0" >> /etc/fstab
mount -a

# Initialize BorgBackup repository
borg init --encryption=none /var/borg/backup/

# Create storage user
adduser --disabled-password --gecos "" storage

# Set up SSH keys for storage user
mkdir -p /home/storage/.ssh
echo "$SSH_KEY_PUBLIC_STAGING" >> /home/storage/.ssh/authorized_keys
echo "$SSH_KEY_PUBLIC_LIVE" >> /home/storage/.ssh/authorized_keys
chown -R storage:storage /home/storage/.ssh
chmod 700 /home/storage/.ssh
chmod 644 /home/storage/.ssh/authorized_keys

# Set up permissions for storage directory
chown -R storage:storage /mnt/storage/

# Configure MySQL client for ddev user
cat <<EOT >> /home/storage/.my.cnf
[client]
host = $MYSQL_SERVER_IP
user = $MYSQL_SERVER_USER
password = $MYSQL_SERVER_PASSWORD
port = $MYSQL_SERVER_PORT
socket = /var/run/mysqld/mysqld.sock
EOT

# Set up cron jobs for BorgBackup
cat <<EOT >> /root/crontab
*/5 * * * * /usr/bin/borg create /var/borg/backup::backup-{now:\%Y-\%m-\%d-\%H-\%M-\%S} /mnt/storage/
*/32 * * * * /usr/bin/borg prune --keep-hourly=120 --keep-daily=60 --keep-weekly=52 --keep-monthly=24 --keep-yearly=10 /var/borg/backup
EOT
crontab < /root/crontab
rm /root/crontab
