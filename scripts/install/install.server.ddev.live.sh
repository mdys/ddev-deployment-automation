#!/bin/bash

# Prompt for necessary information
echo "SSH_KEY_PUBLIC_STAGING:"
read -r SSH_KEY_PUBLIC_STAGING
echo "MYSQL_SERVER_IP:"
read -r MYSQL_SERVER_IP
echo "MYSQL_SERVER_USER:"
read -r MYSQL_SERVER_USER
echo "MYSQL_SERVER_PASSWORD:"
read -r MYSQL_SERVER_PASSWORD
echo "MYSQL_SERVER_PORT:"
read -r MYSQL_SERVER_PORT
echo "Partition 1:"
read -r PARTITION1
echo "Partition 2:"
read -r PARTITION2
echo -e "$MYSQL_SERVER_USER\n$MYSQL_SERVER_PASSWORD\n$MYSQL_SERVER_IP\n$MYSQL_SERVER_PORT\n$SSH_KEY_PUBLIC_STAGING"
echo "Everything okay? (yes/no)"
read -r OKAY

# Check confirmation
if [ "$OKAY" != "yes" ]; then
  echo "Setup aborted."
  exit 1
fi

# Format and mount partitions
mkfs.ext4 "$PARTITION1"
mkfs.ext4 "$PARTITION2"
DISK1=$(blkid -o value -s UUID "$PARTITION1")
DISK2=$(blkid -o value -s UUID "$PARTITION2")
mkdir -p /var/lib/docker
mkdir -p /home/ddev
echo "UUID=$DISK1 /var/lib/docker ext4 defaults 0 0" >> /etc/fstab
echo "UUID=$DISK2 /home/ddev ext4 defaults 0 0" >> /etc/fstab
mount /var/lib/docker
mount /home/ddev

# Setup swap
fallocate -l 8G /swapfile
dd if=/dev/zero of=/swapfile bs=1024 count=8388608
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo "/swapfile swap swap defaults 0 0" >> /etc/fstab

# Install necessary packages
apt-get -y remove docker docker-engine docker.io containerd runc
apt-get update
apt-get -y install ca-certificates curl gnupg lsb-release libnss3-tools sudo bash jq rsync python3-pip sshfs default-mysql-client apache2-utils

# Setup Docker repository
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update
apt-get -y install docker-ce docker-ce-cli containerd.io

# Install Docker Compose
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# Add Docker group and user
addgroup docker
useradd --shell /bin/bash -m -d /home/ddev ddev
usermod -a -G docker ddev
chown -R ddev:ddev /home/ddev
echo 'ddev ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/ddev
chmod 0440 /etc/sudoers.d/ddev

# Setup SSH keys
mkdir -p /home/ddev/.ssh
echo "$SSH_KEY_PUBLIC_STAGING" > /home/ddev/.ssh/authorized_keys
chmod 700 /home/ddev/.ssh
chmod 600 /home/ddev/.ssh/authorized_keys

# Install DDEV
curl -fsSL https://apt.fury.io/drud/gpg.key | gpg --dearmor | tee /etc/apt/trusted.gpg.d/ddev.gpg > /dev/null
echo "deb [signed-by=/etc/apt/trusted.gpg.d/ddev.gpg] https://apt.fury.io/drud/ * *" | tee /etc/apt/sources.list.d/ddev.list
apt update
apt -y install ddev

# Run additional setup as ddev user
runuser -l ddev -c 'mkcert -install'
runuser -l ddev -c 'ssh-keygen -t rsa -N "" -f /home/ddev/.ssh/id_rsa'
runuser -l ddev -c 'echo "0 3 * * * ddev restart --all" | crontab -'

# Update fuse.conf
sed -i "s/#user_allow_other/user_allow_other/g" /etc/fuse.conf

# Configure MySQL client
cat <<EOT >> /home/ddev/.my.cnf
[client]
host = $MYSQL_SERVER_IP
user = $MYSQL_SERVER_USER
password = $MYSQL_SERVER_PASSWORD
port = $MYSQL_SERVER_PORT
socket = /var/run/mysqld/mysqld.sock
EOT

# Display public key for adding to other servers
cat /home/ddev/.ssh/id_rsa.pub
