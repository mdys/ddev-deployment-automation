#!/bin/bash

# Assign arguments to variables
PROJECT_SLUG=$1
STAGING_DOMAIN=$2
STAGING_STORAGE_IP=$3
STAGING_STORAGE_USER=$4
DEPLOY_TYPE=$5
CI_ENVIRONMENT_SLUG=$6

# Get the directory of the currently executing script
SCRIPT_DIR=$(dirname "$0")

# Source the required scripts
source "$SCRIPT_DIR/staging/helper.sh"
source "$SCRIPT_DIR/staging/variables.sh"
source "$SCRIPT_DIR/staging/domainhandling.sh"
source "$SCRIPT_DIR/staging/ddevcreate.sh"
source "$SCRIPT_DIR/staging/sshfs.sh"
source "$SCRIPT_DIR/staging/httpauth.sh"
source "$SCRIPT_DIR/staging/database.sh"

# Check necessary variables
check_variable "$PROJECT_SLUG" "PROJECT_SLUG"
check_variable "$STAGING_DOMAIN" "STAGING_DOMAIN"
check_variable "$STAGING_STORAGE_IP" "STAGING_STORAGE_IP"
check_variable "$STAGING_STORAGE_USER" "STAGING_STORAGE_USER"
check_variable "$DEPLOY_TYPE" "DEPLOY_TYPE"


if [ "${DEPLOY_TYPE}" != "live" ]; then
  check_variable "$CI_ENVIRONMENT_SLUG" "CI_ENVIRONMENT_SLUG"
fi

# Some ddev setups need to be built before running. .ddev/commands/host/deploy holds final build instructions inside project.
if [ -f "$PROJECT_FOLDER/.ddev/commands/host/deploy" ]; then
  echo "Start deploy script, if file deploy exists."

  # Check if DDEV_TYPE is typo3 and execute additional steps
  if [ "$DDEV_TYPE" == "typo3" ] && [ -f "$PROJECT_FOLDER/composer.json" ]; then
    # Workaround for creating AdditionalConfiguration.php otherwise no database connection is available
    ddev composer install && ddev restart && source "$SCRIPT_DIR/staging/typo3.sh"
  fi

  ddev deploy "$PROJECT_NAME_SLUG" "$PROJECT_FOLDER" && source "$SCRIPT_DIR/staging/finisher.sh"
else
  echo "Start ddev container."
  ddev start && source "$SCRIPT_DIR/staging/finisher.sh"
fi
